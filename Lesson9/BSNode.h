#ifndef BSNode_H
#define BSNode_H

#include <string>

using namespace std;
template <class T>
class BSNode
{
public:
	BSNode(T val)
	{
		this->_data = val;
		this->_left = nullptr;
		this->_right = nullptr;
		this->_count = 1;
	}
	BSNode(const BSNode& other)
	{
		this->_data = other._data;
		this->_right = other._right;
		this->_left = other._left;
	}

	~BSNode()
	{
		if (this->_left != nullptr)
		{
			delete this->_left;
		}
		if (this->_right != nullptr)
		{
			delete this->_right;
		}
	}


	void insert(T value)
	{
		if (value > this->_data)
		{
			if (this->_right == nullptr)
			{
				this->_right = new BSNode(value);
			}
			else
			{
				this->_right->insert(value);
			}
		}
		else if (value < this->_data)
		{
			if (this->_left == nullptr)
			{
				this->_left = new BSNode(value);
			}
			else
			{
				this->_left->insert(value);
			}
		}
		else
		{
			this->_count++;
		}
	}

	BSNode& operator=(const BSNode & other)
	{
		return BSNode(other);
	}

	bool isLeaf() const
	{
		return this->_left == nullptr && this->_right == nullptr;
	}

	T getData() const
	{
		return this->_data;
	}

	BSNode * getLeft() const
	{
		return this->_left;
	}

	BSNode * getRight() const
	{
		return this->_right;
	}

	bool search(T val) const
	{
		if (this->_data == val)
		{
			return true;
		}
		if (this->isLeaf())
		{
			return false;
		}
		if (this->_data > val)
		{
			this->_right->search(val);
		}
		else
		{
			this->_left->search(val);
		}
	}

	int getHeight() const
	{
		int r = this->_right != nullptr ? this->_right->getHeight() : 0;
		int l = this->_left != nullptr ? this->_left->getHeight() : 0;

		return 1 + (r > l ? r : l);
	}


	int getDepth(const BSNode & root) const
	{
		return root.getCurrNodeDistFromInputNode(this);
	}


	void printNodes() const
	{
		if (this->isLeaf())
		{
			cout << this->_data << ", ";
		}
		else
		{
			if (this->_left != nullptr)
			{
				this->_left->printNodes();
			}
			cout << this->_data << ", ";
			if (this->_right != nullptr)
			{
				this->_right->printNodes();
			}
		}
	}


private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	
	int getCurrNodeDistFromInputNode(const BSNode * node) const
	{
		if (this->_data == node->_data)
		{
			return 0;
		}
		if (this->isLeaf())
		{
			return -1;
		}
		else
		{
			int count = 1;
			if (this->_data < node->_data)
			{
				int result = this->_right->getCurrNodeDistFromInputNode(node);
				if (result == -1)
				{
					return result;
				}
				else
				{
					count += result;
				}
			}
			else
			{
				int result = this->_left->getCurrNodeDistFromInputNode(node);
				if (result == -1)
				{
					return -1;
				}
				else
				{
					count += result;
				}
			}

			return count;
		}
	}


};

#endif