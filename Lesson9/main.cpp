#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>


using namespace std;


int main()
{
	BSNode<string>* bs = new BSNode<string>("Tair");
	string studs[] = { "Tomer", "Ben", "Robah", "Tal", "Alin", "Alen", "Boris", "Yam", "Almog", "Ilay", "Osher" };
	int studsNum = 11;
	for (int i = 0; i < studsNum; i++)
	{
		bs->insert(studs[i]);
	}
	//Note: i checked this bin tree. drew it on paper. the middle message
	//used to say the node is in depth 5. its actually on depth 3 and depth 5 dont even exist
	//the last one was also wrong. its depth 2 not 3 :)

	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getLeft()->getLeft()->getDepth(*bs) << endl;
	cout << "depth of root: " << bs->getDepth(*bs) << endl;
	cout << "depth of node that is not in the tree: " << bs->getDepth(BSNode<string>("11")) << endl;

	cout << "Printing tree: " << endl;
	bs->printNodes();
	cout << endl;
	
	
	/*
	THAT SHITTY LIB FILE WORKED FINE FOR ONE SINGLE TIME AND THEN STOPPED.
	IDK WHAT TO DO. CANT FIGURE IT OUT
	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	*/
	
	delete bs;

	cout << "\nNow For Int Nodes:" << endl;
	int arrInt[] = { 1,7,2,8,35,64,24,86,3 };
	int arr_size = 9;
	cout << "Before: " << endl;
	for (int i = 0; i < arr_size; i++)
	{
		cout << arrInt[i] << " ";
	}
	cout << "\After: " << endl;
	
	BSNode<int>* sortInt = new BSNode<int>(arrInt[0]);
	for (int i = 1; i < arr_size; i++)
	{
		sortInt->insert(arrInt[i]);
	}

	sortInt->printNodes();
	cout << endl;

	delete sortInt;
	system("pause");

	return 0;
}

