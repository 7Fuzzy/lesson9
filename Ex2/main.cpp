#include "functions.h"
#include <iostream>

using namespace std;

int main() {

	/*
	Somehow i see syntax erorrs.. but when i run the code it works fine
	*/

//check Compare
	std::cout << "correct print is 1 -1 0" << endl;
	std::cout << Compare<double>(1.0, 2.5) << endl;
	std::cout << Compare<char>('X', 'D') << endl;
	std::cout << Compare<double>(4.4, 4.4) << endl;

//check BubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	char charArr[arr_size] = { 'c', '!', 'X', '8', 'd' };
	BubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;
	
	cout << "char array before: " << endl;
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	BubbleSort<char>(charArr, arr_size);

//check PrintArray
	std::cout << "correct print is sorted array" << std::endl;
	PrintArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	PrintArray<char>(charArr, arr_size);
	system("pause");
	return 1;
}