#pragma once

template <class T>
int Compare(T x, T y)
{
	if (x == y)
	{
		return 0;
	}
	else if (x < y)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}


template <class T>
void BubbleSort(T* arr, int len)
{
	for (int i = 0; i < len; i++)
	{
		for (int j = 0; j < len - i - 1; j++)
		{
			if (Compare(arr[i + 1], arr[i]) > 0)
			{
				T temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;
			}
		}
	}
}

template <class T>
void PrintArray(T* arr, int len)
{
	for (int i = 0; i < len; i++)
	{
		cout << arr[i] << endl;
	}
}
